// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------

const rule = require('../../../lib/rules/vue-emit-events-with-two-args');
const RuleTester = require('eslint').RuleTester;

// ------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------

const ruleTester = new RuleTester();

const error = 'Expected emit to be called with no more than two arguments.';

ruleTester.run('vue-emit-events-with-two-args', rule, {
  valid: [
    '$emit("some");',
    'emit("some");',
    'vm.$emit("some");',
    'vm.emit("some");',
    '$emit("some", { key: ["val1", "val2"] });',
    'emit("some", { key: ["val1", "val2"] });',
    'vm.$emit("some", { key: ["val1", "val2"] });',
    'vm.emit("some", { key: ["val1", "val2"] });',
  ],

  invalid: [
    '$emit("some", "other1", "other2");',
    'emit("some", "other1", "other2");',
    'vm.$emit("some", "other1", "other2");',
    'vm.emit("some", "other1", "other2");',
  ].map((code) => ({ code, errors: [{ type: 'CallExpression', message: error }] })),
});
