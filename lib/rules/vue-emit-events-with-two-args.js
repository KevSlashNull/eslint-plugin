// ------------------------------------------------------------------------------
// Requirements
// ------------------------------------------------------------------------------
const { DOCS_BASE_URL } = require('../constants');
const { getFunctionName } = require('../utils/rule-utils');

// ------------------------------------------------------------------------------
//   Helpers
// ------------------------------------------------------------------------------

const EMIT_LIST = new Set(['$emit', 'emit']);
const ERROR_MESSAGE = `Expected emit to be called with no more than two arguments.`;

// ------------------------------------------------------------------------------
// Rule Definition
// ------------------------------------------------------------------------------

module.exports = {
  meta: {
    type: 'problem',
    docs: {
      description: `Throw an error when emit is called with more than two arguments.`,
      url: DOCS_BASE_URL + '/vue-emit-events-with-two-args.md',
    },
  },
  create(context) {
    function report(node) {
      context.report({
        node,
        message: ERROR_MESSAGE,
      });
    }

    function isEmitCall(node) {
      const functionName = getFunctionName(node);

      return functionName && EMIT_LIST.has(functionName);
    }

    function checkFunctionArguments(callExpression) {
      if (callExpression.arguments.length > 2) {
        report(callExpression);
      }
    }

    return {
      CallExpression(node) {
        if (isEmitCall(node)) {
          checkFunctionArguments(node);
        }
      },
    };
  },
};
