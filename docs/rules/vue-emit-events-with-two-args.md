# @gitlab/vue-emit-events-with-two-args

Throw an error when emit is called with more than two arguments.

## Rule Details

It has been decided to use `mitt` as an event bus implementation and this library does not support argument list as the event arguments, [it expects two arguments only](https://github.com/developit/mitt#emit).

To pass an argument list, an object should be used as the second argument.

### Examples of **incorrect** code for this rule

`emit` method called with more than two arguments.

```javascript
vm.$emit('keyspressed', 'enter', '1');
vm.emit('keyspressed', 'enter', '1');
```

### Examples of **correct** code for this rule

Pass an object as the second argument to `emit`.

```javascript
vm.$emit('keyspressed', { keys: ['enter', '1'] });
vm.emit('keyspressed', { keys: ['enter', '1'] });
```

`emit` method called with one argument.

```javascript
vm.$emit('focus');
vm.emit('focus');
```

## Options

Nothing
